import sys
import timeit
from collections import defaultdict

from math import log2
from math import sqrt

from csv_reader.CsvReader import read_csv_file

from evaluation.RandomForestEvaluation import RandomForestEvaluation


def print_features(feature_used):
    for feature in feature_used:
        print("Feature " + str(feature[0]) + " has been used " + str(feature[1]) + " times")


def print_result(NT, F, scores, feature_used):
    print('Trees: %d' % NT)
    print('F: %d' % F)
    print('Scores: %s' % scores)
    print('Mean Accuracy: %.2f%%' % (sum(scores) / float(len(scores))))
    print('Feature Used most')
    for feature in feature_used:
        print("Feature " + str(feature[0]) + " has been used " + str(feature[1]) + " times")


F = None
NT_F_combinations = None
K = 5

# If parameters are provided we initialize F and NT as desired by the user
# Instead, if they are not provided we run the algorithm over all the possible combinations
# provided in the exercise assignment
if len(sys.argv) > 1 and sys.argv[1] is not None:
    dataset_path = sys.argv[1]
    if len(sys.argv) >= 4:
        F = float(sys.argv[2])
        NT = int(sys.argv[3])
        if len(sys.argv) > 4:
            K = int(sys.argv[4])
    elif len(sys.argv) == 3:
        K = int(sys.argv[2])
else:
    print("Error, missing parameters")
    print("Usage: python RandomForest.py <database path> [<F> <NT>] [K]")
    dataset_path = "dataset/wine/wine.data.txt"

dataset = read_csv_file(dataset_path)

# Initialize all the combinations if no F and NT provided
if F is None:
    M = len(dataset[0]) - 1  # total number of features
    F = sqrt(M)
    NT_F_combinations = [(50, 1),
                         (50, 3),
                         (50, int(log2(M) + 1)),
                         (50, sqrt(M)),
                         (100, 1),
                         (100, 3),
                         (100, int(log2(M) + 1)),
                         (100, sqrt(M))]

if NT_F_combinations is None:

    start_time = timeit.default_timer()

    evaluation = RandomForestEvaluation(K, F, NT)
    scores, feature_used = evaluation.evaluate_random_forest(dataset, True)

    print_result(NT, F, scores, feature_used)
    elapsed = timeit.default_timer() - start_time
    print("Elapsed time: %.3f seconds" % elapsed)
else:
    ordered_scores = dict()
    start_time = timeit.default_timer()
    feature_used_rank = defaultdict(lambda: 0)
    for NT_F in NT_F_combinations:
        evaluation = RandomForestEvaluation(K, NT_F[1], NT_F[0])
        scores, feature_used = evaluation.evaluate_random_forest(dataset, True)
        mean_accuracy = (sum(scores) / float(len(scores)))
        print_result(NT_F[0], NT_F[1], scores, feature_used)
        ordered_scores[NT_F] = mean_accuracy
        for feature in feature_used:
            feature_used_rank[feature[0]] += feature[1]

    ordered_scores = [(k, ordered_scores[k]) for k in sorted(ordered_scores, key=ordered_scores.get, reverse=True)]
    ordered_features = [(k, feature_used_rank[k]) for k in
                        sorted(feature_used_rank, key=feature_used_rank.get, reverse=True)]

    print()
    print("---- Final summary ----")
    elapsed = timeit.default_timer() - start_time
    print("Elapsed time: %.3f seconds" % elapsed)
    print("Best scores")
    for nt_f, score in ordered_scores:
        print("NT = " + str(nt_f[0]) + " F = " + str(nt_f[1]) + " mean_accuracy = " + str(score))
    print_features(ordered_features)
