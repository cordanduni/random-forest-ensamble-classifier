import csv


def read_csv_file(csv_file,delimiter=','):
    with open(csv_file, 'rt') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=delimiter)
        dataset = list()
        for row in csvreader:
            if not row:
                continue
            dataset.append(row)
        return dataset
