from collections import defaultdict
from random import randrange

from machine_learning.RandomForest import RandomForest


class RandomForestEvaluation:

    def __init__(self, n_folds, n_features, n_trees):
        self.n_features = n_features
        self.n_trees = n_trees
        self.n_folds = n_folds
        self.feature_used_map = defaultdict(lambda: 0)

    def evaluate_random_forest(self, dataset, should_evaluate_num_features_used):
        folds = self.cross_validation_split(dataset, self.n_folds)
        scores = list()
        ordered_frequencies = dict()

        for fold in folds:
            train_set = list(folds)
            train_set.remove(fold)
            train_set = sum(train_set, [])
            test_set = list()
            for row in fold:
                row_copy = list(row)
                test_set.append(row_copy)
                row_copy[-1] = None
            randomForest = RandomForest(train_set,test_set,should_evaluate_num_features_used)
            predicted = randomForest.random_forest(self.n_trees, self.n_features)
            actual = [row[-1] for row in fold]
            accuracy = self.accuracy_metric(actual, predicted)
            scores.append(accuracy)
            if should_evaluate_num_features_used:
                for key in randomForest.feature_used_map.keys():
                    self.feature_used_map[key] += randomForest.feature_used_map[key]
        if should_evaluate_num_features_used:
            ordered_frequencies = [(k, self.feature_used_map[k]) for k in sorted(self.feature_used_map, key=self.feature_used_map.get, reverse=True)]
        return scores,ordered_frequencies

    # Creates the splits for th ecross validation
    @staticmethod
    def cross_validation_split(dataset, n_folds):
        dataset_split = list()
        dataset_copy = list(dataset)
        fold_size = int(len(dataset) / n_folds)
        for i in range(n_folds):
            fold = list()
            while len(fold) < fold_size:
                index = randrange(len(dataset_copy))
                fold.append(dataset_copy.pop(index))
            dataset_split.append(fold)
        return dataset_split

    # Calculate accuracy
    @staticmethod
    def accuracy_metric(actual, predicted):
        correct = 0
        for i in range(len(actual)):
            if actual[i] == predicted[i]:
                correct += 1
        return correct / float(len(actual)) * 100.0