from collections import defaultdict
from math import sqrt
from random import randrange

from machine_learning.tree.Tree import Tree


class RandomForest:
    train_set, test_set = None, None
    feature_used_map = defaultdict(lambda: 0)

    def __init__(self, train_set, test_set, should_evaluate_num_features_used):
        self.train_set = train_set
        self.test_set = test_set
        self.feature_used_map = defaultdict(lambda: 0)
        self.should_evaluate_num_features_used = should_evaluate_num_features_used

    # Make a prediction with a decision tree
    def predict(self, node, row):
        if row[node.feature_index] < node.value:
            if isinstance(node.left, Tree):
                return self.predict(node.left, row)
            else:
                return node.left.value
        else:
            if isinstance(node.right, Tree):
                return self.predict(node.right, row)
            else:
                return node.right.value

    # Create a random subsample with replacement
    def random_subsample(self):
        sample = list()
        n_sample = round(len(self.train_set))
        while len(sample) < n_sample:
            index = randrange(len(self.train_set))
            sample.append(self.train_set[index])
        return sample

    # Make a prediction with a list of bagged trees
    def bagging_predict(self, trees, row):
        predictions = [self.predict(tree, row) for tree in trees]
        return max(set(predictions), key=predictions.count)

    # Random Forest Algorithm
    def random_forest(self, n_trees, n_features):
        trees = list()
        for i in range(n_trees):
            sample = self.random_subsample()  # random subsample with replacement
            tree = Tree(sample, n_features, should_evaluate_num_features_used=self.should_evaluate_num_features_used)
            trees.append(tree)
            if self.should_evaluate_num_features_used:
                for key in tree.feature_used_map.keys():
                    self.feature_used_map[key] += tree.feature_used_map[key]
        predictions = [self.bagging_predict(trees, row) for row in self.test_set]
        return predictions
