from collections import defaultdict

from machine_learning.tree.Leaf import Leaf
from machine_learning.utils.gini import calc_gini_index
from random import randrange

CLASS_INDEX = -1
MIN_NUM_ELEMENT_FOR_SPLIT = 1
FEATURE_INDEX = "f_index"
FEATURE_VALUE = "f_value"
SPLIT = "split"
GINI_INDEX = "gini_i"

MAX_DEPTH = 10
class Tree:

    root = None
    index = 0
    left,right = None,None
    splits = (left,right)
    value = 0
    gini_index = 9999
    feature_index = -1
    feature_used_map = defaultdict(lambda: 0)

    def __init__(self, train_set, n_features, max_depth = MAX_DEPTH, depth = 1, should_evaluate_num_features_used = False):
        self.should_evaluate_num_features_used = should_evaluate_num_features_used
        self.feature_used_map = defaultdict(lambda: 0)
        self.get_split(train_set, n_features)
        self.build_subtree(self, max_depth, n_features, depth)



    def build_subtree(self, tree, max_depth, n_features, depth):
        left, right = tree.splits
        tree.splits = (None,None)  # We remove the split from the central node
        # check for a no split
        if not left or not right:
            tree.left = tree.right = self.create_leaf(left + right)
            return
        # check for max depth
        if depth >= max_depth:
            tree.left, tree.right = self.create_leaf(left), self.create_leaf(right)
            return
        # process left child
        if len(left) <= MIN_NUM_ELEMENT_FOR_SPLIT:
            tree.left = self.create_leaf(left)
        else:
            tree.left = Tree(left, n_features,max_depth,depth + 1 )
        # process right child
        if len(right) <= MIN_NUM_ELEMENT_FOR_SPLIT:
            tree.right = self.create_leaf(right)
        else:
            tree.right = Tree(right, n_features,max_depth,depth + 1 )

    @staticmethod
    def create_leaf(group):
        outcomes = [row[CLASS_INDEX] for row in group]
        return Leaf(max(set(outcomes), key=outcomes.count))

    # Decides the split point
    def get_split(self, dataset, n_features):
        class_values = list(set(row[CLASS_INDEX] for row in dataset))
        feature_index, feature_value, splits = 999, 999, None
        lowest_gini_score = 99999
        features = list()
        while len(features) < n_features:
            index = randrange(len(dataset[0])-1)
            if index not in features:
                features.append(index)
        for index in features:
            for row in dataset:
                split = self.split_by_value(index, row[index], dataset)
                gini = calc_gini_index(split, class_values)
                if gini < lowest_gini_score:
                    feature_index = index
                    feature_value = row[index]
                    lowest_gini_score = gini
                    splits = split
        self.splits = splits
        self.value = feature_value
        self.gini_index = lowest_gini_score
        self.feature_index = feature_index
        if self.should_evaluate_num_features_used:
            self.feature_used_map[feature_index] += 1

    @staticmethod
    def split_by_value(index, value, dataset):
        left, right = list(), list()
        for row in dataset:
            if row[index] < value:
                left.append(row)
            else:
                right.append(row)
        return left, right

