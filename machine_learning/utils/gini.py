CLASS_INDEX = -1

def calc_gini_index(groups, classes):
    # As described in https://en.wikipedia.org/wiki/Decision_tree_learning
    gini = 0.0
    for group in groups:
        size = float(len(group))
        if size == 0:
            continue
        p_sum = 0.0
        for class_ in classes:
            p = [row[CLASS_INDEX] for row in group].count(class_) / size
            p_sum += p * p
        gini += p_sum
    return 1.0 - gini